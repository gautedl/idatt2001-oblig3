package edu.ntnu.idatt2001.oblig3.gautedl;
import org.junit.Test;
import org.junit.jupiter.api.*;

import java.util.ArrayList;

public class DeckOfCardsTest {

    @Test
    @DisplayName("Creating a deck of cards")
    public void TestCreatingADeckOfCards(){
        DeckOfCards deckOfCards = new DeckOfCards();
        Assertions.assertEquals(52, deckOfCards.getCards().size());

    }

    @Nested
    @DisplayName("Deals a random hand")
    class DealHand{
        @Test
        public void DealAHandOfTenCards(){
            DeckOfCards deckOfCards = new DeckOfCards();
            ArrayList<PlayingCard> handOfCards = deckOfCards.dealHand(10);

            Assertions.assertEquals(10, handOfCards.size());
        }
        @Test
        public void DealTwoRandomHandsOfFiveCards(){
            ArrayList<PlayingCard> handOfCards = new ArrayList<PlayingCard>();
            DeckOfCards deckOfCards = new DeckOfCards();
            handOfCards = deckOfCards.dealHand(5);
            ArrayList<PlayingCard> handOfCards2 = deckOfCards.dealHand(5);

            Assertions.assertFalse(handOfCards.equals(handOfCards2));
        }

    }
}
