package edu.ntnu.idatt2001.oblig3.gautedl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.*;

import java.util.List;

public class HandOfCardsTest {

    @Test
    @DisplayName("Check if a hand of cards have flush")
    public void handOfCardsHasFlush(){
        PlayingCard card1 = new PlayingCard('S', 4);
        PlayingCard card2 = new PlayingCard('S', 5);
        PlayingCard card3 = new PlayingCard('S', 7);
        PlayingCard card4 = new PlayingCard('S', 10);
        PlayingCard card5 = new PlayingCard('S', 11);

        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(card1);
        handOfCards.addCard(card2);
        handOfCards.addCard(card3);
        handOfCards.addCard(card4);
        handOfCards.addCard(card5);
        Assertions.assertTrue(handOfCards.flush());


    }
    @Test
    @DisplayName("Check if a hand of five cards dont have flush.")
    public void handOfFiveCardsDoNotHaveFlush(){
        PlayingCard card1 = new PlayingCard('H', 4);
        PlayingCard card2 = new PlayingCard('S', 5);
        PlayingCard card3 = new PlayingCard('S', 7);
        PlayingCard card4 = new PlayingCard('S', 10);
        PlayingCard card5 = new PlayingCard('S', 11);

        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(card1);
        handOfCards.addCard(card2);
        handOfCards.addCard(card3);
        handOfCards.addCard(card4);
        handOfCards.addCard(card5);
        Assertions.assertFalse(handOfCards.flush());
    }

    @Test
    @DisplayName("Check if a hand has queen of spades.")
    public void handHasQueenOfSpades(){
        PlayingCard card1 = new PlayingCard('H', 4);
        PlayingCard card2 = new PlayingCard('S', 5);
        PlayingCard card3 = new PlayingCard('S', 7);
        PlayingCard card4 = new PlayingCard('S', 10);
        PlayingCard card5 = new PlayingCard('S', 12);

        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(card1);
        handOfCards.addCard(card2);
        handOfCards.addCard(card3);
        handOfCards.addCard(card4);
        handOfCards.addCard(card5);
        Assertions.assertTrue(handOfCards.queenOfSpades());
    }
    @Test
    @DisplayName("Check sum of hand.")
    public void SumOfHandOfCards(){
        PlayingCard card1 = new PlayingCard('H', 4);
        PlayingCard card2 = new PlayingCard('S', 5);
        PlayingCard card3 = new PlayingCard('S', 7);
        PlayingCard card4 = new PlayingCard('S', 10);
        PlayingCard card5 = new PlayingCard('S', 11);

        HandOfCards handOfCards = new HandOfCards();
        handOfCards.addCard(card1);
        handOfCards.addCard(card2);
        handOfCards.addCard(card3);
        handOfCards.addCard(card4);
        handOfCards.addCard(card5);
        Assertions.assertEquals(37, handOfCards.sumOfCards());
    }
}
