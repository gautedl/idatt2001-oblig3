package edu.ntnu.idatt2001.oblig3.gautedl;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

import java.util.ArrayList;

/**
 * Controller class that controlls the application
 * @Author Gaute Degré Lorentsen
 * @Version 1.0.0 05.04.2021
 */
public class AppController {
    private final DeckOfCards deckOfCards = new DeckOfCards();
    private final HandOfCards handOfCards = new HandOfCards();

    @FXML
    public Button dealHandButton;
    @FXML
    public Label dealHandText;
    @FXML
    public Button checkHandButton;
    @FXML
    public Label flushText;
    @FXML
    public Label queenOfSpades;
    @FXML
    public Label sumOfFaces;
    @FXML
    public Label cardsOfHearth;

    //Amount of cards dealt
    int n = 5;

    /**
     * Method for the "Deal Hand" button.
     * Deals a new hand of cards and prints to a text area.
     */
    public void dealHand(){
        handOfCards.clearDeck();
        handOfCards.addDeck(deckOfCards.dealHand(n));

        dealHandText.setText(handOfCards.toString());
    }

    /**
     * Method for the check hand button.
     * Checks if the hand has flush, queen of spades, all of the hearts and calculates the sum.
     */
    public void checkHand(){
        if (handOfCards.flush()){
            flushText.setText("Yes");
        } else {
            flushText.setText("No");
        }

        sumOfFaces.setText(String.valueOf(handOfCards.sumOfCards()));

        if(handOfCards.queenOfSpades()){
            queenOfSpades.setText("Yes");
        } else {
            queenOfSpades.setText("no");
        }

        cardsOfHearth.setText(handOfCards.getHearts().toString());

    }

}
