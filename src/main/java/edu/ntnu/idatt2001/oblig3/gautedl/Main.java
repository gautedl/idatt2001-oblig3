package edu.ntnu.idatt2001.oblig3.gautedl;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *Main method.
 * @Author Gaute Degré Lorentsen
 * Version 1.0.0 05.04.2021
 */

public class Main extends Application {

    /**
     * Launches a new stage from Mainfxml.fxml file, where the code for the gui is.
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/Mainfxml.fxml"));
        stage.setTitle("Cardgame");
        stage.setScene(new Scene(root));
        stage.show();

    }

    /**
     * Launches the program.
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
