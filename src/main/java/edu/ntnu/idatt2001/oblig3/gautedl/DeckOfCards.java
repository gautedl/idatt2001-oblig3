package edu.ntnu.idatt2001.oblig3.gautedl;
import java.util.ArrayList;
import java.util.Random;

/**
 * The class for controlling a deck of cards
 * @Author Gaute Degré Lorentsen
 * @Version 1.0.0 05.04.2021
 */
public class DeckOfCards {

    private final ArrayList<PlayingCard> cards;
    private final char[] suits = {'S', 'H', 'D', 'C'};



    /**
     *Constructor for adding a deck of unique cards.
     */
    public DeckOfCards() {
        cards = new ArrayList<>();
        for (char c : suits){
            for (int i = 1; i <= 13; i++){
                if(!cards.contains( new PlayingCard(c,i))){
                    cards.add(new PlayingCard(c,i));
                }

            }
        }
    }


    /**
     * Get method for getting the list of cards.
     * @return cards
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Method for dealing a hand of cards.
     * Thorws argument if amount of cards is less than 1 or more than 52.
     * @param int n. N represent the number of cards dealt.
     * @return
     */
    public ArrayList<PlayingCard> dealHand(int n){
        if (n < 1 || n > 52){
            throw new IllegalArgumentException("Card amount cannot be less than 1 or more than 52.");
        }
        Random random = new Random();
        ArrayList<PlayingCard> handOfCards = new ArrayList<PlayingCard>();
        for (int i = 0; i < n; i++){
            int rNumber = random.nextInt(13);
            int rSuit = random.nextInt(4);

            handOfCards.add(new PlayingCard(suits[rSuit], rNumber + 1)); //Add the card to the list.

        }

        return handOfCards;
    }

    /**
     * To-String method
     * @return cards
     */
    @Override
    public String toString() {
        return "DeckOfCards{" +
                "cards=" + cards +
                '}';
    }
}

