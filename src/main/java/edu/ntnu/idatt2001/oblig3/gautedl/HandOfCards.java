package edu.ntnu.idatt2001.oblig3.gautedl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class for dealing with a hand of cards.
 * @Author Gaute Degré Lorentsen
 * @Version 1.0.0 05.04.2021
 */
public class HandOfCards {
    private ArrayList<PlayingCard> cards = new ArrayList<PlayingCard>();

    /**
     * Constructor for hand of cards. Creates a new list.
     */
    public HandOfCards() {
        this.cards = new ArrayList<>(cards);
    }

    /**
     * Adds a card to the hand of cards list.
     * @param newCard
     */
    public void addCard (PlayingCard newCard){
        cards.add(newCard);
    }

    /**
     * Adds a deck of cards to the hand of cards.
     * @param newDeck
     */
    public void addDeck (ArrayList<PlayingCard> newDeck){
        for (PlayingCard card : newDeck){
            cards.add(card);
        }

    }

    /**
     * Clears the hannd of cards.
     */
    public void clearDeck (){
        cards.clear();
    }

    /**
     * Get method for cards.
     * @return list of cards.
     */
    public ArrayList<PlayingCard> getCards() {
        return cards;
    }

    /**
     * Get method for getting all the spades in the hand.
     * @return List of spades
     */
    public List<PlayingCard> getSpades(){
        return cards.stream().filter(playingCard -> playingCard.getSuit()== 'S').collect(Collectors.toList());
    }
    /**
     * Get method for getting all the hearts in the hand.
     * @return List of hearts
     */
    public List<PlayingCard> getHearts(){
        return cards.stream().filter(playingCard -> playingCard.getSuit()== 'H').collect(Collectors.toList());
    }
    /**
     * Get method for getting all the Diamonds in the hand.
     * @return List of diamonds
     */
    public List<PlayingCard> getDiamond(){
        return cards.stream().filter(playingCard -> playingCard.getSuit()== 'D').collect(Collectors.toList());
    }
    /**
     * Get method for getting all the clubs in the hand.
     * @return List of clubs
     */
    public List<PlayingCard> getClubs(){
        return cards.stream().filter(playingCard -> playingCard.getSuit()== 'C').collect(Collectors.toList());
    }

    /**
     * Checks if the hand has queen of spades.
     * @return true if hand has queen of spades, and false if hand doesn't
     */
    public boolean queenOfSpades(){
        if(cards.stream().anyMatch(playingCard -> playingCard.getSuit()== 'S' && playingCard.getFace() == 12)){
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Checks if the hand has flush. 5 or more of one of the suits.
     * @return true if hand has flush, and false if it doesn't
     */
    public boolean flush() {

        if (getSpades().size() >= 5){
            return true;
        } else if (getClubs().size() >= 5){
            return true;
        }else if (getHearts().size() >= 5){
            return true;
        }
        else if (getDiamond().size() >= 5){
            return true;
        }
        else {
            return false;
        }

    }

    /**
     * Calculates the sum of the faces
     * @return int with the sum of the faces
     */
    public int sumOfCards(){
        return cards.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * To-string method
     * @return
     */
    @Override
    public String toString() {
        return cards + "\n";
    }
}
